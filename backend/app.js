var express = require('express')
var app = express()
var db = require('./db')
var pacienteMsgController = require('./pacienteMsg/pacienteMsgController')


app.use('/msgs', pacienteMsgController)

module.exports = app