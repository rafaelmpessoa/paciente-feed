var app = require('./app')
var port = process.env.PORT || 3000
const rotamed_db = require('./rotamed_db')
const sql = require('mssql')
const pacienteMsg = require('./pacienteMsg/pacienteMsg')

var server = app.listen(port, function () {
    console.log('Servidor escutando a porta ' + port)
})



const sqlConect = sql.connect(rotamed_db[0].config)

function sincronizar(conexao,clienteNome) {
    const ListaObj = []
    conexao.then(() => {
        return sql.query`select * from hospital`
    }).then(result => {
        const obj = Array.from(result.recordsets[0])
        obj.forEach(val => {
            ListaObj.push({
                id: val.HOS_CODIGO,
                pacienteId: val.HOS_CODIGO,
                pacienteNome: 'Paciente ' + val.HOS_CODIGO,
                msgString: val.HOS_NOME,
                msgDtHora: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                clienteToken: '999999'
            })


        })

        pacienteMsg.insertMany(ListaObj, function (err) {
            console.log('Dados da ' + clienteNome + ' Inseridos!')
        })

    }).catch(err => {
        console.log(err.message)
    })

    // console.log(ListaObj)


}

const objSqlConect = []
function connectAllClients(rotamed_db) {
    let pool
    rotamed_db.forEach((val) => {

        pool = new sql.ConnectionPool(val.config)
        teste = pool.connect(err => {
            if (!err) {
                console.log('Conexão feita com ' + val.cliente)
                objSqlConect.push(sql.connect(val.config))
            } else {
                console.log('Não foi possivel se conectar com ' + val.cliente)
                console.log(err.code + ': ' + err.message)
            }

        })
    })

    
}

//connectAllClients(rotamed_db)


sincronizar(sqlConect,'Qualivida')