var mongoose = require('mongoose')

var pacienteMsgSchema = new mongoose.Schema({
    id: String,
    pacienteId: String,
    pacienteNome: String,
    msgString: String,
    msgDtHora: String,
    clienteToken: String
})

mongoose.model('pacienteMsg', pacienteMsgSchema)

module.exports = mongoose.model('pacienteMsg')