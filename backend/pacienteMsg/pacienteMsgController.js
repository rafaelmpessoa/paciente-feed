
var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({extended: true}))
router.use(bodyParser.json())


var pacienteMsg = require('./pacienteMsg')


router.post('/', function (req, res) {
    pacienteMsg.create({
        id: req.body.id,
        pacienteId: req.body.pacienteId,
        pacienteNome: req.body.pacienteNome,
        msgString: req.body.msgString,
        msgDtHora: req.body.msgDtHora,
        clienteToken: req.body.clienteToken
        }, 
        function (err, value) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(value);
        });
});

router.get('/', function(req,res) {
    pacienteMsg.find({}, function(err,msgs){
        if(err) return res.status(500).send("Não foi possivel encontrar as mensagens")
        res.status(200).send(msgs)
    })


})

router.get('/:cliToken/:pacId', function(req,res) {
    pacienteMsg.find({
            pacienteId: req.params.pacId,
            clienteToken: req.params.cliToken
        }, 
            function(err,msgs){
        if(err) return res.status(500).send("Não foi possivel encontrar as mensagenss para o paciente")
        if(!msgs) return res.status(404).send('Paciente não encontrado')
        res.status(200).send(msgs)
    })


})




module.exports = router